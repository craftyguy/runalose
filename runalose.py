#!/usr/bin/env python3
#
# Copyright (C) 2018-2019 Clayton Craft <clayton@craftyguy.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import attr
import geohash
import json
from lxml import etree
import os
import sys
from time import gmtime, strftime
# Progress indicator is neat, but optional
try:
    from tqdm import tqdm
    prog_indicate = True
except ImportError:
    prog_indicate = False


@attr.s(slots=True)
class Training(object):
    id = attr.ib()
    sportid = attr.ib()
    # time is in epoch format
    time: int = attr.ib()
    timezone_offset: int = attr.ib()
    # s is time in seconds
    s = attr.ib()
    distance = attr.ib()
    elevation = attr.ib()
    activity_id = attr.ib()
    routeid = attr.ib()
    kcal = attr.ib()
    power = attr.ib()
    notes = attr.ib()
    pulse_avg = attr.ib()
    pulse_max = attr.ib()


@attr.s(slots=True)
class Trackdata(object):
    id = attr.ib()
    time = attr.ib()
    distance = attr.ib()
    heartrate = attr.ib()
    cadence = attr.ib()
    power = attr.ib()

    def get_data_time(self):
        return gmtime(self.time)


@attr.s(slots=True)
class Route(object):
    id = attr.ib()
    name = attr.ib()
    distance = attr.ib()
    elevation = attr.ib()
    elevation_up = attr.ib()
    elevation_down = attr.ib()
    geohashes = attr.ib()
    elevations_original = attr.ib()
    elevations_corrected = attr.ib()
    startpoint = attr.ib()
    endpoint = attr.ib()
    route_min = attr.ib()
    route_max = attr.ib()


def split_data_by_pipe(data):
    """ Splits data by a | delimiter, returns list or None
    if data is empty """
    return data.split('|') if data else None


def extend_geohashes(geohashes):
    """ Extend geohash coordinates to their full length.
    Runalyze truncates (from front) coordinates to save space(?) """
    # TODO: This makes python cry.
    # Loop is faster than list comp..
    # First item in the list is the full, extended geohash
    # Shortened geohashes depend on the preceeding extended hash for
    # determining the what the leading values should be for extending it
    new_geohashes = [geohashes[0]]
    prev_gh = geohashes[0]
    for i in range(len(geohashes)):
        gh = prev_gh[0:(12 - len(geohashes[i]))] + geohashes[i]
        prev_gh = gh
        new_geohashes.append(gh)
    return new_geohashes


def epoch_to_datetime(epoch, tz_offset_m):
    """ Converts epoch (local time, in seconds) to xml's dateTime format
    tz_offset_m should be minutes from GMT to offset """
    time = gmtime(epoch - (tz_offset_m * 60))
    return strftime("%Y-%m-%dT%H:%M:%SZ", time)


def main():
    # TODO: make these commandline options
    # TODO: autolap stuff is not implemented yet
    auto_lap = False
    auto_lap_meters = 1000
    # TODO: single training export not implemented yet
    single_training = None
    #single_training = '3373600'

    argp = argparse.ArgumentParser(description=('Convert training events from a '
                                                'runalyze json backups into TCX '
                                                'files'))
    argp.add_argument('json_file', metavar='<json file>', action='store',
                      help=('Runalyze json backup file to process.'))
    argp.add_argument('--out-dir', action='store', default='/tmp/runalose',
                      help=('Directory to output TCX files [default: '
                            '%(default)s]'))
    argp.add_argument('--out-prefix', action='store', default='runalose_',
                      help=('Prefix to use for TCX files [default: '
                            '%(default)s]'))
    argp.add_argument('--table-prefix', action='store', default='runalyze_',
                      help=('Prefix used by the runalyze tables in the backup '
                            '[default: %(default)s]'))

    args = argp.parse_args()

    # runalyze 'backup' json is not really a valid json file
    # so we have to read in the whole file and parse it line by line
    # where each line is a 'valid' json thing
    try:
        f = open(args.json_file)
    except OSError:
        assert False, 'Unable to open the json backup file: %s' % args.json_file
    line = f.readline()
    trackdata = {}
    sports = {}
    trainings = {}
    routes = {}
    schema_ver = ''

    try:
        os.mkdir(args.out_dir)
    except FileExistsError:
        pass

    while line:
        if 'version' in line:
            _, schema_ver = line.rstrip().split('=')
        if 'TABLE' in line:
            table_line = json.loads(line)
            table_name = table_line['TABLE']
            if table_name == args.table_prefix + 'sport':
                line = f.readline()
                while line != '\n':
                    sport_row = json.loads(line)
                    sport_id = list(sport_row.keys())[0]
                    sports[sport_id] = sport_row[sport_id]['name']
                    line = f.readline()
            elif table_name == args.table_prefix + 'trackdata':
                # this table stores track info for an activity
                line = f.readline()
                while line != '\n':
                    td_row = json.loads(line)
                    # there is no track data ID, only an activity id
                    td_id = td_row[""]['activityid']
                    d = td_row[""]
                    td = Trackdata(td_id,
                                   split_data_by_pipe(d['time']),
                                   split_data_by_pipe(d['distance']),
                                   split_data_by_pipe(d['heartrate']),
                                   split_data_by_pipe(d['cadence']),
                                   split_data_by_pipe(d['power']))
                    trackdata[td_id] = td
                    line = f.readline()
            elif table_name == args.table_prefix + 'route':
                # contains geohashes of route and elevation data
                line = f.readline()
                while line != '\n':
                    r_row = json.loads(line)
                    r_id = list(r_row.keys())[0]
                    d = r_row[r_id]
                    r = Route(r_id,
                              split_data_by_pipe(d['name']),
                              split_data_by_pipe(d['distance']),
                              split_data_by_pipe(d['elevation']),
                              split_data_by_pipe(d['elevation_up']),
                              split_data_by_pipe(d['elevation_down']),
                              split_data_by_pipe(d['geohashes']),
                              split_data_by_pipe(d['elevations_original']),
                              split_data_by_pipe(d['elevations_corrected']),
                              split_data_by_pipe(d['startpoint']),
                              split_data_by_pipe(d['endpoint']),
                              split_data_by_pipe(d['min']),
                              split_data_by_pipe(d['max']))
                    routes[r_id] = r
                    line = f.readline()
            elif table_name == args.table_prefix + 'training':
                # this contains activity info
                line = f.readline()
                while line != '\n':
                    t_row = json.loads(line)
                    t_id = list(t_row.keys())[0]
                    t = Training(t_id,
                                 t_row[t_id]['sportid'],
                                 int(t_row[t_id]['time']),
                                 int(t_row[t_id]['timezone_offset']),
                                 t_row[t_id]['s'],
                                 t_row[t_id]['distance'],
                                 t_row[t_id]['elevation'],
                                 t_row[t_id]['activity_id'],
                                 t_row[t_id]['routeid'],
                                 t_row[t_id]['kcal'],
                                 t_row[t_id]['power'],
                                 t_row[t_id]['notes'],
                                 t_row[t_id]['pulse_avg'],
                                 t_row[t_id]['pulse_max'])
                    trainings[t_id] = t
                    line = f.readline()
        line = f.readline()
    f.close()

    ns_map = {None: 'http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2',
              'xsi': "http://www.w3.org/2001/XMLSchema-instance",
              'ext': "http://www.garmin.com/xmlschemas/ActivityExtension/v2",
              'xsd': "http://www.w3.org/2001/XMLSchema"
              }

    # List of trainings to iterate through (one, fancy [all], plain [all])
    if single_training:
        training_list = [single_training]
    elif prog_indicate:
        training_list = tqdm(trainings.keys(), ascii=True, unit='activities',
                             bar_format=(' {percentage:3.0f}% |{bar}| {n_fmt}'
                                         '/{total_fmt} [{elapsed}<{remaining}, '
                                         '{rate_fmt} {postfix}] '))
    else:
        training_list = trainings.keys()

    for training in training_list:
        t = trainings[training]
        # If no route, time val, or distance, don't add training event
        if not t.routeid or not trackdata[t.id].time or not t.distance:
            continue
        root = etree.Element("TrainingCenterDatabase", nsmap=ns_map)
        activities = etree.SubElement(root, 'Activities', nsmap=ns_map)

        activity = etree.SubElement(activities, 'Activity',
                                    Sport=sports[t.sportid],
                                    nsmap=ns_map)
        # runalyze stores time as localtime, which is why the timezone offset
        # is subtracted from the time rather than added to it.
        id = etree.SubElement(activity, 'Id', nsmap=ns_map)
        id.text = epoch_to_datetime(t.time, t.timezone_offset)
        # when adding autolap, time needs to be from first trackpoint..
        lap = etree.SubElement(activity, 'Lap', nsmap=ns_map,
                               StartTime=epoch_to_datetime(t.time,
                                                           t.timezone_offset))
        total_time_seconds = etree.SubElement(lap, 'TotalTimeSeconds')
        total_time_seconds.text = str(t.s)
        distance_meters = etree.SubElement(lap, 'DistanceMeters')
        distance_meters.text = str(float(t.distance) * 1000)
        if t.kcal:
            calories = etree.SubElement(lap, 'Calories')
            calories.text = str(t.kcal)
        # Not sure where to get values for intensity, so assume all
        # points are active
        intensity = etree.SubElement(lap, 'Intensity')
        intensity.text = 'Active'
        triggermethod = etree.SubElement(lap, 'TriggerMethod')
        if auto_lap:
            triggermethod.text = 'Distance'
        else:
            triggermethod.text = 'Manual'
        if t.pulse_avg:
            average_heart_rate = etree.SubElement(lap, 'AverageHeartRateBpm')
            average_heart_rate.text = str(t.pulse_avg)
        if t.pulse_max:
            max_heart_rate = etree.SubElement(lap, 'MaximumHeartRateBpm')
            max_heart_rate.text = str(t.pulse_max)
        track = etree.SubElement(lap, 'Track')
        geohashes = extend_geohashes(routes[t.routeid].geohashes)
        # Fill out Trackpoints
        prev_time = t.time
        for point in range(len(trackdata[t.id].distance)):
            tp = etree.SubElement(track, 'Trackpoint')
            tp_time = etree.SubElement(tp, 'Time')
            # Time is stored in json as the delta between current and previous
            # time
            cur_time = prev_time + int(trackdata[t.id].time[point])
            tp_time.text = epoch_to_datetime(cur_time, t.timezone_offset)
            prev_time = cur_time
            pos = etree.SubElement(tp, 'Position')
            lat = etree.SubElement(pos, 'LatitudeDegrees')
            lon = etree.SubElement(pos, 'LongitudeDegrees')
            lat.text, lon.text = geohash.decode(geohashes[point])
            if routes[t.routeid].elevations_corrected:
                alt = etree.SubElement(tp, 'AltitudeMeters')
                alt.text = routes[t.routeid].elevations_corrected[point]
            if trackdata[t.id].heartrate:
                hr = etree.SubElement(tp, 'HeartRateBpm')
                hr_v = etree.SubElement(hr, 'Value')
                hr_v.text = trackdata[t.id].heartrate[point]
            if trackdata[t.id].distance:
                dis = etree.SubElement(tp, 'DistanceMeters')
                dis.text = str(round(
                    float(trackdata[t.id].distance[point]) * 1000, 3))
            if trackdata[t.id].cadence:
                cad = etree.SubElement(tp, 'Cadence')
                cad.text = trackdata[t.id].cadence[point]
            if trackdata[t.id].power:
                ext = etree.SubElement(tp, 'Extensions')
                pwr = etree.SubElement(ext, 'Watts')
                pwr.text = trackdata[t.id].power[point]

        with open('%s/%s%s.tcx' % (args.out_dir, args.out_prefix, t.id), 'w') as f:
            f.write(etree.tostring(root, xml_declaration=True,
                                   encoding='utf8').decode())

if __name__ == '__main__':
    main()
