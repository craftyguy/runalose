## Runalose

### Description
This tool converts a runalyze json database backup file into separate TCX files
that can be used in other applications. Runalyze (currently) does not have a
way for users to produce backups of their data that can be used by other
applications, or even within runalyze itself. This aims to help folks backup
their data in a usable format, for either peace of mind or migrating away from
runalyze.

This tool outputs TCX files into a directory of your choice. The following
features are not implemented yet, but are planned some day:

[ ] Compress output TCX files

[ ] Backup single activity (based on activity ID in database backup)

[ ] Auto lap, with user-specified distance (runalyze destroys pre-existing lap
data on import)


This has been tested on my ~1000 activity backup, but all activities are
'Biking'. There's no reason this shouldn't work with other activities (e.g.
'Running'), so please report any issues if you have any!


### Installation

This tool requires Python 3, specifically tested with 3.6, *might* work on
older versions, but older versions are untested.

The following Python modules are also used:

- python-attrs
- python-geohash
- python-lxml
- python-tqdm (optional, but gives a fancy progress bar if installed)

On Arch Linux, all modules except python-geohash are in the official repos.
python-geohash can be found in AUR.

### Usage
```
usage: runalose.py [-h] [--out-dir OUT_DIR] [--out-prefix OUT_PREFIX]
                   [--table-prefix TABLE_PREFIX]
		   <json file>

Convert training events from a runalyze json backups into TCX files

positional arguments:
    <json file>          Runalyze json backup file to process.

optional arguments:
    -h, --help           show this help message and exit
    --out-dir OUT_DIR    Directory to output TCX files [default: /tmp/runalose]
    --out-prefix         OUT_PREFIX
                    	 Prefix to use for TCX files [default: runalose_]
    --table-prefix       TABLE_PREFIX
                         Prefix used by the runalyze tables in the backup
                         [default: runalyze_]

```



### License

GPLv3.

[Unlike the runalyze developers](https://github.com/Runalyze/Runalyze/issues/952#issuecomment-385695306), this project has a clear (and permissive) license.


